
# SETUP

## Install 

sudo apt-get install python3-venv    # If needed
python3 -m venv venv

Open VSCode in de venv

python -m pip install django (or python3 ?)

## Start a project

django-admin startproject web_project . -> Create a project
python3 manage.py startapp hello -> Create an app

## Run server

python3 manage.py runserver