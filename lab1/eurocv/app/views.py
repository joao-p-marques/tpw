import os

import json
from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'eurocv.html')

def personal(request, num):
    file = os.path.dirname(__file__)
    cv_url = os.path.join(file, f"../eurocv_res/eurocv_{num}.json")

    obj = json.loads(open(cv_url).read())['eurocv']
    return render(request, 'eurocv.html', obj)