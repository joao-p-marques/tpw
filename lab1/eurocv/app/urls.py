
from app import views
from django.urls import path

urlpatterns = [
    path('', views.home, name="home"),
    path('person/<int:num>/', views.personal, name="personal_info"),
]
