from django.http import HttpResponse, HttpRequest
from django.shortcuts import render

# Create your views here.

def hello_world(request):
    return HttpResponse("Hello World!")

def new_view(request, num):
    resp = f"<html><body><h1>{num}</h1></body></html>"
    return HttpResponse(resp)