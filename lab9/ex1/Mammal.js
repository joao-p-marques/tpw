///<reference path="Animal.ts"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Mammal = /** @class */ (function (_super) {
    __extends(Mammal, _super);
    function Mammal(sound) {
        var _this = _super.call(this, 'Land') || this;
        _this.nMammals = 0;
        _this.nMammals++;
        _this.sound = sound;
        return _this;
    }
    Mammal.prototype.show = function () {
        return 'Mammal - ${this.habitat} (${this.nMammals}/${this.nAnimals})';
    };
    Mammal.prototype.talk = function () {
        return 'I am a Mammal and my sound is ${this.sound}';
    };
    return Mammal;
}(Animal));
