///<reference path="Feline.ts"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Cat = /** @class */ (function (_super) {
    __extends(Cat, _super);
    function Cat(name) {
        var _this = _super.call(this, 'Cats') || this;
        _this.nCats = 0;
        _this.nCats++;
        _this.name = name;
        return _this;
    }
    Cat.prototype.show = function () {
        return 'Cat ${this.name} - ${this.habitat} (${this.nMammals}/${this.nAnimals})';
    };
    Cat.prototype.talk = function () {
        return 'I am a Cat and my sound is ${this.sound}';
    };
    return Cat;
}(Feline));
