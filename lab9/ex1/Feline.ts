///<reference path="Mammal.ts"/>

class Feline extends Mammal {
    nFelines : number = 0;
    family : string;

    constructor(family : string) {
        super('Miau');
        this.family = family;
        this.nFelines++;
    }

    show () {
        return 'Feline - ${this.habitat} (${this.nMammals}/${this.nAnimals})'
    }

}
