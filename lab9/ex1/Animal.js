var Animal = /** @class */ (function () {
    function Animal(habitat) {
        this.nAnimals = 0;
        this.habitat = habitat;
        this.nAnimals++;
    }
    return Animal;
}());
