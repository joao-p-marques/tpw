
class Animal {
    nAnimals : number = 0;
    habitat : string;

    constructor(habitat : string) {
        this.habitat = habitat;
        this.nAnimals++;
    }

}
