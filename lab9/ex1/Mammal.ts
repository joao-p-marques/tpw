///<reference path="Animal.ts"/>

class Mammal extends Animal{
    nMammals : number = 0;
    sound : string;

    constructor(sound : string) {
        super('Land');
        this.nMammals++;
        this.sound = sound;
    }

    show () {
        return 'Mammal - ${this.habitat} (${this.nMammals}/${this.nAnimals})'
    }

    talk () {
        return 'I am a Mammal and my sound is ${this.sound}'
    }

}