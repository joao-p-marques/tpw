///<reference path="Feline.ts"/>

class Cat extends Feline {
    nCats : number = 0;
    name : string;

    constructor(name : string) {
        super('Cats');
        this.nCats++;
        this.name = name;
    }

    show () {
        return 'Cat ${this.name} - ${this.habitat} (${this.nMammals}/${this.nAnimals})'
    }

    talk () {
        return 'I am a Cat and my sound is ${this.sound}'
    }
}