from app import views
from django.urls import path

urlpatterns = [
    path('listbooks/', views.listbooks, name="listbooks"),
    path('book/<int:book_id>', views.book, name="book"),
    path('author/<int:author_id>', views.author, name="author"),
    path('publisher/<int:publisher_id>', views.publisher, name="publisher"),
]
