from django.shortcuts import render

# Create your views here.
from app.models import *


def home(request):
    return render(request, '<h1>Hello</h1>')

def listbooks(request):
    books_list = Book.objects.all()
    return render(request, 'list_books.html', {"books": books_list})

def book(request, book_id):
    book = Book.objects.get(id = book_id)
    return render(request, 'book.html', {"book" : book})

def author(request, author_id):
    author = Author.objects.get(id = author_id)
    author_books = list(Book.objects.filter(authors=author))
    return render(request, 'author.html', {"author" : author, "books" : author_books})

def publisher(request, publisher_id):
    publisher = Publisher.objects.get(id = publisher_id)
    publisher_books = list(Book.objects.filter(publisher = publisher))
    return render(request, 'publisher.html', {"publisher" : publisher, "books" : publisher_books})
