from django.db import models

# Create your models here.

from typing import List


class Address:
    street: str
    cp: str
    local: str
    country: str

    def __init__(self, street: str, cp: str, local: str, country: str) -> None:
        self.street = street
        self.cp = cp
        self.local = local
        self.country = country


class Birthdate:
    day: str
    month: str
    year: int

    def __init__(self, day: str, month: str, year: int) -> None:
        self.day = day
        self.month = month
        self.year = year


class Email:
    type: str
    value: str

    def __init__(self, type: str, value: str) -> None:
        self.type = type
        self.value = value


class Fax:
    countryprefix: int
    number: int

    def __init__(self, countryprefix: int, number: int) -> None:
        self.countryprefix = countryprefix
        self.number = number


class PersonalInfoContact:
    phone: Fax
    fax: Fax
    email: List[Email]

    def __init__(self, phone: Fax, fax: Fax, email: List[Email]) -> None:
        self.phone = phone
        self.fax = fax
        self.email = email


class Name:
    lastname: str
    firstname: str

    def __init__(self, lastname: str, firstname: str) -> None:
        self.lastname = lastname
        self.firstname = firstname


class PersonalInfo:
    name: Name
    address: Address
    contact: PersonalInfoContact
    nacionality: str
    birthdate: Birthdate
    gender: str

    def __init__(self, name: Name, address: Address, contact: PersonalInfoContact, nacionality: str, birthdate: Birthdate, gender: str) -> None:
        self.name = name
        self.address = address
        self.contact = contact
        self.nacionality = nacionality
        self.birthdate = birthdate
        self.gender = gender


class Activity:
    designation: str
    time: str

    def __init__(self, designation: str, time: str) -> None:
        self.designation = designation
        self.time = time


class Activities:
    activity: List[Activity]

    def __init__(self, activity: List[Activity]) -> None:
        self.activity = activity


class End:
    month: str
    year: int

    def __init__(self, month: str, year: int) -> None:
        self.month = month
        self.year = year


class Dates:
    start: End
    end: End

    def __init__(self, start: End, end: End) -> None:
        self.start = start
        self.end = end


class EmployerContact:
    www: str
    email: str

    def __init__(self, www: str, email: str) -> None:
        self.www = www
        self.email = email


class Employer:
    name: str
    address: Address
    contact: EmployerContact

    def __init__(self, name: str, address: Address, contact: EmployerContact) -> None:
        self.name = name
        self.address = address
        self.contact = contact


class Position:
    dates: Dates
    occupation: str
    activities: Activities
    employer: Employer
    business_sector: str

    def __init__(self, dates: Dates, occupation: str, activities: Activities, employer: Employer, business_sector: str) -> None:
        self.dates = dates
        self.occupation = occupation
        self.activities = activities
        self.employer = employer
        self.business_sector = business_sector


class Workexperience:
    position: Position

    def __init__(self, position: Position) -> None:
        self.position = position


class Person:
    foto: str
    desired_employ: str
    personal_info: PersonalInfo
    workexperience: Workexperience

    def __init__(self, foto: str, desired_employ: str, personal_info: PersonalInfo, workexperience: Workexperience) -> None:
        self.foto = foto
        self.desired_employ = desired_employ
        self.personal_info = personal_info
        self.workexperience = workexperience

