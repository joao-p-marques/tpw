
import {Author} from "./author";

export const AUTHORS: Author[] = [
  { num : 1, name : 'Fernando Pessoa', email : 'fpessoa@mail.pt' },
  { num : 2, name : 'J. K. Rolling', email : 'jkrolling@mail.uk' },
  { num : 3, name : 'Stephen King', email : 'sking@mail.com' },
  { num : 4, name : 'Arthur Conan Doyle', email : 'acdoyle@mail.net' },
  { num : 5, name : 'John Green', email : 'jgreen@mail.com' },
];

